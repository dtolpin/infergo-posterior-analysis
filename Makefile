COMMANDS=ess describe

all: $(COMMANDS)

%: cmd/%/main.go
	go build ./cmd/$@

install: 
	for c in $(COMMANDS); do \
		go install ./cmd/$$c; \
	done

clean:
	rm -f $(COMMANDS)
