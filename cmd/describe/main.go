package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"sort"
	"strconv"
)

func init() {
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(),
			`Compute summary statistics for each column:
		describe [OPTIONS] [QUANTILES] < samples.csv`)
		flag.PrintDefaults()
	}
}

var (
	QUANTILES = []float64{0.05, 0.25, 0.5, 0.75, 0.95}
)

func main() {
	flag.Parse()

	if flag.NArg() != 0 {
		QUANTILES = []float64{}
		for i := 0; i != flag.NArg(); i++ {
			q, err := strconv.ParseFloat(flag.Arg(i), 64)
			switch {
			case err != nil:
				log.Fatalf("cannot parse quantile %q: %v",
					flag.Arg(i), err)
			case q < 0 || q >= 1:
				log.Fatalf(
					"quantile is %f, but must be in [0, 1)", q)
			}
			QUANTILES = append(QUANTILES, q)
		}
	}

	rdr := csv.NewReader(os.Stdin)
	var samples [][]float64 // columnwise
	for {
		record, err := rdr.Read()
		if err == io.EOF {
			break
		}

		if samples == nil {
			samples = make([][]float64, len(record))
		}

		for i := range record {
			value, err := strconv.ParseFloat(record[i], 64)
			if err != nil {
				log.Fatalf("invalid data %q: %v", record[i], err)
			}
			samples[i] = append(samples[i], value)
		}
	}

	// collect summary statistics for columns, to output
	// them side-by-side
	var stats [][]float64
	for i := range samples {
		stats = append(stats, describe(samples[i], QUANTILES))
	}

	var rownames []string
	rownames = append(rownames, "mean", "sd", "lowest")
	for _, q := range QUANTILES {
		var rowname string
		if q == 0.5 {
			rowname = "median"
		} else {
			rowname = fmt.Sprintf("%.0f%%", q*100)
		}
		rownames = append(rownames, rowname)
	}
	rownames = append(rownames, "highest")

	for j := range rownames {
		fmt.Print(rownames[j])
		for i := range stats {
			fmt.Printf(",%f", stats[i][j])
		}
		fmt.Println()
	}
}

func describe(
	samples []float64,
	quantiles []float64,
) (stat []float64) {
	n := float64(len(samples))

	s, s2 := 0., 0.
	for _, x := range samples {
		s += x
		s2 += x * x
	}
	mean := s / float64(n)
	std := math.Sqrt(s2/float64(n) - mean*mean)
	stat = append(stat, mean)
	stat = append(stat, std)

	sort.Float64s(samples)
	// lowest
	stat = append(stat, samples[0])
	for _, p := range quantiles {
		j := int(math.Floor(n * p))
		stat = append(stat, samples[j])
	}
	// highest
	stat = append(stat, samples[len(samples)-1])

	return stat
}
