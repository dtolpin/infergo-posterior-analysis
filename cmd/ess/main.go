package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"gonum.org/v1/gonum/fourier"
	"io"
	"log"
	"math/cmplx"
	"os"
	"strconv"
)

func init() {
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(),
			`Compute ESS for each column:
		ess [OPTIONS] < samples.csv`)
		flag.PrintDefaults()
	}
}

func main() {
	flag.Parse()

	if flag.NArg() != 0 {
		log.Fatalf("unexpected positional arguments: %v",
			flag.Args())
	}

	rdr := csv.NewReader(os.Stdin)
	var samples [][]float64 // columnwise
	for {
		record, err := rdr.Read()
		if err == io.EOF {
			break
		}

		if samples == nil {
			samples = make([][]float64, len(record))
		}

		for i := range record {
			value, err := strconv.ParseFloat(record[i], 64)
			if err != nil {
				log.Fatalf("invalid data %q: %v", record[i], err)
			}
			samples[i] = append(samples[i], value)
		}
	}

	for i := range samples {
		fmt.Printf("%f", ess(samples[i]))
		if i == len(samples)-1 {
			fmt.Println()
		} else {
			fmt.Print(",")
		}
	}
}

func ess(samples []float64) float64 {
	n := float64(len(samples))

	// center the samples
	m := 0.
	for _, x := range samples {
		m += x
	}
	m /= n
	for i := range samples {
		samples[i] -= m
	}

	// pad with zeroes
	k := 1
	for k < len(samples) {
		k *= 2
	}
	padded := append(samples,
		make([]float64, 2*k-len(samples))...)

	// compute cross-correlations as ifft(fft*fft)
	fft := fourier.NewFFT(len(padded))
	coeff := fft.Coefficients(nil, padded)
	for i := range coeff {
		coeff[i] *= cmplx.Conj(coeff[i])
	}
	rhos := fft.Sequence(nil, coeff)

	// compute effective sample size, stop when
	// the sum of two consecutive cross-correlations
	// becomes negative
	sum := 0.
	for i := 1; i < len(samples)-1; i += 2 {
		p := rhos[i] + rhos[i+1]
		if p < 0 {
			break
		}
		// cross-corellations are not normalized
		// and must be divided by cross-corellation at 0
		sum += p / rhos[0]
	}
	return n / (1. + sum)
}
